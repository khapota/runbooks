<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Create a Visualisation based on a search in Discover](#create-a-visualisation-based-on-a-search-in-discover)
- [Get percentiles of x requests](#get-percentiles-of-x-requests)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# Create a Visualisation based on a search in Discover

# Get percentiles of x requests
