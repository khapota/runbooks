[
  import 'api.jsonnet',
  import 'ci-runners.jsonnet',
  import 'git.jsonnet',
  import 'gitaly.jsonnet',
  import 'monitoring.jsonnet',
  import 'nfs.jsonnet',
  import 'registry.jsonnet',
  import 'sidekiq.jsonnet',
  import 'web.jsonnet',
]
